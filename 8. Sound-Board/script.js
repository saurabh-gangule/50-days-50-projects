const sounds = ['grenade', 'bomb', 'wind', 'door', 'walk', 'dog'];

sounds.forEach(sound => {
    const btn = document.createElement('button');
    btn.classList.add('btn');

    btn.innerText = sound.toUpperCase();

    btn.addEventListener('click', () => {
        stopPrevSongs();
        document.querySelector(`#${sound}`).play();
    })

    document.querySelector('#btns').appendChild(btn);
})

function stopPrevSongs() {
    sounds.forEach(sound => {
        const song = document.querySelector(`#${sound}`);
        song.pause();
        song.currentTime = 0;
    })
}