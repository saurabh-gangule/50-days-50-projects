const counter = document.querySelector('.counter');
const img = document.querySelector('.img-bg');
let count = 0;

let interval = setInterval(blurring, 40);

function blurring() {
    count++;
    if (count > 100) {
        clearInterval(interval);
    }
    else {
        counter.textContent = `${count} %`;
    counter.style.opacity = scale(count, 0, 100, 1, 0);
    img.style.filter =  `blur(${scale(count, 0, 100, 30, 0)}px)`;
    }
}

const scale = (num, in_min, in_max, out_min, out_max) => {
    return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}