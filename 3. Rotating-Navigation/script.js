const open = document.querySelector('#open-btn');
const close = document.querySelector('#close-btn');
const jumboContainer = document.querySelector('.jumbo-container');

open.addEventListener('click', () => jumboContainer.classList.add('show-nav'));
close.addEventListener('click', () => jumboContainer.classList.remove('show-nav'));