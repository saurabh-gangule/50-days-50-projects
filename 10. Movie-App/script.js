const movieAPI = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c&page=1';
const imageAPI = 'https://image.tmdb.org/t/p/w500';
const serachAPI = 'https://api.themoviedb.org/3/search/movie?api_key=3fd2be6f0c70a2a598f084ddfb75487c&query="';

const search = document.querySelector('#search');
const form = document.querySelector('#form');
const main = document.querySelector('#main');

fetchMovies(movieAPI);

async function fetchMovies(url) {
    const movies = await fetch(url);
    const movieData = await movies.json();

    loadMovies(movieData);
}

function loadMovies(data) {
    main.innerHTML = '';
    data.results.forEach(el => {
        const { poster_path, title, overview, vote_average } = el;
        const movieCard = document.createElement('div');
        movieCard.classList.add('movie-card');
        movieCard.innerHTML = `
        <div class="movie-image">
            <img src="${imageAPI+poster_path}" alt="${title}">
        </div>
        <div class="movie-title">
            <h2>${title}</h2>
            <span class="${getRatingType(vote_average)}">${vote_average}</span>
        </div>
        <div class="movie-overview">
            <h3>Overview</h3>
            <p>
                ${overview}
            </p>
        </div>
        `;
        main.appendChild(movieCard);
    });
}

function getRatingType(rating) {
    if (rating >= 8) {
        return 'green';
    } else if (rating >= 5) {
        return 'orange';
    } else {
        return 'red';
    }
}

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const searchTerm = search.value;
    if(searchTerm && searchTerm !== '') {
        fetchMovies(serachAPI + searchTerm);
        search.value = '';
    } else {
        window.location.reload();
    }
})